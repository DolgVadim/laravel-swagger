<?php

namespace Cdonut\LaravelSwagger;

use ReflectionMethod;
use Illuminate\Support\Str;
use Illuminate\Routing\Route;
use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\DocBlockFactory;
use  phpDocumentor\Reflection\DocBlock\Tags\Generic;

class Generator
{
    protected $config;

    protected $routeFilter;

    protected $docs;

    protected $uri;

    protected $originalUri;

    protected $method;

    protected $action;

    public function __construct($config, $routeFilter = null)
    {
        $this->config = $config;
        $this->routeFilter = $routeFilter;
        $this->docParser = DocBlockFactory::createInstance();
    }

    public function generate()
    {
        $this->docs = $this->getBaseInfo();
        foreach ($this->getAppRoutes() as $route) {
            $this->originalUri = $uri = $this->getRouteUri($route);
            $this->uri = strip_optional_char($uri);

            if ($this->routeFilter && !preg_match('/^' . preg_quote($this->routeFilter, '/') . '/', $this->uri)) {
                continue;
            }

            $excludeRoutes = $this->config['excludePath'] ?: null;

            if (!empty($excludeRoutes) && in_array($this->uri, $excludeRoutes)) {
                continue;
            }

            $this->action = $route->getAction()['uses'];
            $methods = $route->methods();

            if (!isset($this->docs['paths'][$this->uri])) {
                $this->docs['paths'][$this->uri] = [];
            }

            foreach ($methods as $method) {
                $this->method = strtolower($method);

                if (in_array($this->method, $this->config['ignoredMethods'])) continue;

                $this->generatePath();
            }
        }

        return $this->docs;
    }

    protected function getBaseInfo()
    {
        $baseInfo = [
            'swagger' => '2.0',
            'info' => [
                'title' => $this->config['title'],
                'description' => $this->config['description'],
                'version' => $this->config['appVersion'],
            ],
            'host' => $this->config['host'],
            'basePath' => $this->config['basePath'],
        ];

        if (!empty($this->config['schemes'])) {
            $baseInfo['schemes'] = $this->config['schemes'];
        }

        if (!empty($this->config['consumes'])) {
            $baseInfo['consumes'] = $this->config['consumes'];
        }

        if (!empty($this->config['produces'])) {
            $baseInfo['produces'] = $this->config['produces'];
        }

        if (!empty($this->config['security'])) {
            $baseInfo['securityDefinitions'] = $this->config['security'];
        }

        $baseInfo['paths'] = [];

        return $baseInfo;
    }

    protected function getAppRoutes()
    {
        return app('router')->getRoutes();
    }

    protected function getRouteUri(Route $route)
    {
        $uri = $route->uri();

        if (!Str::startsWith($uri, '/')) {
            $uri = '/' . $uri;
        }

        return $uri;
    }

    protected function generatePath()
    {
        $actionInstance = is_string($this->action) ? $this->getActionClassInstance($this->action) : null;
        $docBlock = $actionInstance ? ($actionInstance->getDocComment() ?: "") : "";
        list($isDeprecated, $summary, $description, $tags, $params, $isSecured) = $this->parseActionDocBlock($docBlock);

        $this->docs['paths'][$this->uri][$this->method] = [
            'summary' => $summary,
            'description' => $description,
            'deprecated' => $isDeprecated,
            'responses' => [
                '200' => [
                    'description' => 'OK'
                ]
            ],
        ];
        if (!empty($tags))
            $this->docs['paths'][$this->uri][$this->method]['tags'] = $tags;

        if ($isSecured) {
            $schemes = [];
            foreach ($this->config['security'] as $key => $config) {
                $schemes[$key] = [];
            }
            $this->docs['paths'][$this->uri][$this->method]['security'][] = $schemes;
        }

        $this->addActionParameters($params);
    }

    protected function addActionParameters($docParams = [])
    {
        $rules = []; // $this->getFormRules() ?: [];

        $parameters = (new Parameters\PathParameterGenerator($this->originalUri))->getParameters();

        if (!empty($rules)) {
            $parameterGenerator = $this->getParameterGenerator($rules);

            $parameters = array_merge($parameters, $parameterGenerator->getParameters());
        }

        if (!empty($docParams)) {
            foreach ($docParams as $docParam) {
                if (strpos($docParam['type'], 'array.') !== false) {
                    $typeParts = explode('.', $docParam['type']);
                    $docParam['type'] = 'array';
                    $docParam['items']['type'] = $typeParts[1];
                    $docParam['collectionFormat'] = 'multi';
                }
                $key = array_search($docParam['name'], array_column($parameters, 'name'));
                if ($key !== false) {
                    $parameters[$key] = $docParam;
                } elseif ($docParam['in'] == 'body') {
                    $this->addToBody($parameters, $docParam);
                } else {
                    $parameters[] = $docParam;
                }
            }
        }

        if (!empty($parameters)) {
            $this->docs['paths'][$this->uri][$this->method]['parameters'] = $parameters;
        }
    }

    protected function addToBody(&$parameters, $param)
    {
        $bodyKey = array_search('body', array_column($parameters, 'in'));
        if ($bodyKey === false) {
            $parameters[] = [
                'in' => 'body',
                'name' => 'body',
                'description' => '',
                'schema' => [
                    'type' => 'object',
                ]
            ];
            $bodyKey = array_search('body', array_column($parameters, 'in'));
        }

        if ($param['required'] == true) {
            if (empty($parameters[$bodyKey]['schema']['required']))
                $parameters[$bodyKey]['schema']['required'][] = $param['name'];
            elseif (!in_array($param['name'], $parameters[$bodyKey]['schema']['required']))
                $parameters[$bodyKey]['schema']['required'][] = $param['name'];
        }


        $parameters[$bodyKey]['schema']['properties'][$param['name']] = [
            'type' => $param['type'],
            'description' => $param['description']
        ];

        if ($param['type'] == 'array')
            $parameters[$bodyKey]['schema']['properties'][$param['name']]['items']['type'] = $param['items']['type'];

        if (!empty($param['format']))
            $parameters[$bodyKey]['schema']['properties'][$param['name']]['format'] = $param['format'];

    }

    protected function getFormRules()
    {
        if (!is_string($this->action)) return false;

        $parameters = $this->getActionClassInstance($this->action)->getParameters();

        foreach ($parameters as $parameter) {
            $class = (string)$parameter->getType();

            if (is_subclass_of($class, FormRequest::class)) {
                return (new $class)->rules();
            }
        }
    }

    protected function getParameterGenerator($rules)
    {
        switch ($this->method) {
            case 'post':
            case 'put':
            case 'patch':
                return new Parameters\BodyParameterGenerator($rules);
            default:
                return new Parameters\QueryParameterGenerator($rules);
        }
    }

    private function getActionClassInstance(string $action)
    {
        list($class, $method) = Str::parseCallback($action);

        return new ReflectionMethod($class, $method);
    }

    private function parseActionDocBlock(string $docBlock)
    {
        if (empty($docBlock) || !$this->config['parseDocBlock']) {
            return [false, "", "", [], [], false];
        }

        try {
            $parsedComment = $this->docParser->create($docBlock);

            $docParams = $parsedComment->getTagsByName('swaggerParam');

            $params = $this->parseDocParams($docParams);

            $docTags = $parsedComment->getTagsByName('swaggerTag');
            $tags = [];
            foreach ($docTags as $tag) {
                $tags[] = trim((string)$tag, " \t\n\r\0\x0B()");
            }

            $tags = array_unique($tags);
            $isDeprecated = $parsedComment->hasTag('deprecated');

            $isSecured = $parsedComment->hasTag('swaggerSecurity');

            $summary = $parsedComment->getSummary();
            $description = (string)$parsedComment->getDescription();

            return [$isDeprecated, $summary, $description, $tags, $params, $isSecured];
        } catch (\Exception $e) {
            return [false, "", "", [], [], false];
        }
    }

    protected function parseDocParams($docParams)
    {
        $params = [];
        foreach ($docParams as $paramObject) {
            $rawParam = trim((string)$paramObject, '() ');
            $prop = [];
            $settings = explode(',', $rawParam);
            foreach ($settings as $settingKey => $setting) {
                if (strpos($setting, '=') === false) {
                    $searchIndex = $settingKey;
                    do {
                        $searchIndex--;
                        if (!isset($settings[$searchIndex]))
                            continue;

                        if (strpos($settings[$searchIndex], '=') !== false) break;
                    } while ($searchIndex >= 0);

                    if (!empty($settings[$searchIndex])) {
                        $settings[$searchIndex] .= ',' . $setting;
                        unset($settings[$settingKey]);
                    }
                }
            }
            $settings = array_values($settings);
            foreach ($settings as $param) {
                if (empty($param)) continue;
                $parts = explode('=', $param);
                $propKey = trim($parts[0], " *\t\n\r\0\x0B");
                $propValue = trim($parts[1], " *\t\n\r\0\x0B");
                if ($propKey == 'required')
                    $propValue = $propValue == 'true';
                $prop[$propKey] = $propValue;
            }
            $params[] = $prop;
        }
        return $params;
    }
}